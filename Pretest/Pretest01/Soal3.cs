﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest01
{
    internal class Soal3
    {
        public Soal3()
        {
            Console.Write("Input : ");
            int jmlh = int.Parse(Console.ReadLine());
            for (int i = 0; i < jmlh; i++)
            {
                Console.Write("Masukkan urutan angka (kasih spasi) : ");
                string[] angka = Console.ReadLine().Split(' ');
                int[] randomArr = Array.ConvertAll<string, int>(angka, int.Parse);
                Array1Dim(Array.ConvertAll(randomArr, x => x.ToString()));
                Console.WriteLine();
                int[] sortedArr = Insertion(randomArr);
                Array1Dim(Array.ConvertAll(sortedArr, x => x.ToString()));
            }
        }
        public static int[] Insertion(int[] arr)
        {
            //int[] result = new int[arr.Length];
            for (int i = 1; i < arr.Length; i++)
            {
                for (int j = i; j > 0; j--)
                {
                    if (arr[j] < arr[j - 1])
                    {
                        var temp = arr[j - 1];
                        arr[j - 1] = arr[j];
                        arr[j] = temp;
                    }
                }
            }
            return arr;
        }
        public static void Array1Dim(string[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write($"{arr[i]}\t");
            }
        }
    }
}
