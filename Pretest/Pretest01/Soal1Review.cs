﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest01
{
    internal class Soal1Review
    {
        public Soal1Review()
        {
            //Topi 10 7 19
            //Kemeja 20 25 18
            Console.WriteLine("Pretest Soal 01 Review");
            Console.Write("Topi : ");
            int[] topiHarga = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
            Console.Write("Kemeja : ");
            int[] kemejaHarga = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);

            int terkecil = topiHarga[0] + kemejaHarga[0];
            int terbesar = terkecil;

            for (int topi = 0; topi < topiHarga.Length; topi++)
            {
                for (int kemeja = 0; kemeja < kemejaHarga.Length; kemeja++)
                {
                    if (terkecil > topiHarga[topi] + kemejaHarga[kemeja])
                        terkecil = topiHarga[topi] + kemejaHarga[kemeja];
                    

                    if (terbesar < topiHarga[topi] + kemejaHarga[kemeja])
                        terbesar = topiHarga[topi] + kemejaHarga[kemeja];
                }
            }
            Console.WriteLine($"Terkecil {terkecil}, Terbesar {terbesar}");
        }
    }
}
