﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest01
{
    internal class Soal1
    {
        public Soal1()
        {
            Console.Write("Harga Topi (pakai spasi \' \') : ");
            int[] itemTopi = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);
            Console.Write("Harga Kemeja (pakai spasi\' \') : ");
            int[] itemKemeja = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

            int maxBelanja = 0, minBelanja = 0;
            for (int i = 0; i < itemTopi.Length; i++)
            {
                for (int j = 0; j < itemKemeja.Length; j++)
                {
                    int hargaMin = ((itemKemeja[i] + itemTopi[j]) + (itemKemeja[i] - itemTopi[j])) / 2;
                    if (hargaMin >= minBelanja)
                    {
                        minBelanja = hargaMin;
                    }

                    int harga = itemTopi[i] + itemKemeja[j];
                    if (harga >= maxBelanja)
                    {
                        maxBelanja = harga;
                    }

                }
            }
            Console.WriteLine(minBelanja + " " + maxBelanja);
        }
    }
}
