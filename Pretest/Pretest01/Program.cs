﻿namespace Pretest01
{
    public class Program
    {
        public Program()
        {
            menu();
        }
        static void Main(string[] args)
        {
            menu();
        }
        static void menu()
        {
            Console.WriteLine("==== Welcome to Pretest 01 ====");
            Console.WriteLine("|   1. Soal 1                  |");
            Console.WriteLine("|   2. Soal 1 Review           |");
            Console.WriteLine("|   3. Soal 2                  |");
            Console.WriteLine("|   4. Soal 2 Review           |");
            Console.WriteLine("|   5. Soal 3                  |");
            Console.WriteLine("|   6. Soal 3 Review           |");
            Console.WriteLine("|   0. Exit		       |");
            Console.Write("Masukkan no soal: ");
            int soal = int.Parse(Console.ReadLine());

            switch (soal)
            {
                case 1:
                    Soal1 soal1 = new Soal1();
                    kembali();
                    break;
                case 2:
                    Soal1Review soal1Review = new Soal1Review();
                    kembali();
                    break;
                case 3:
                    Soal2 soal2 = new Soal2();
                    kembali();
                    break;
                //case 4:
                //    Soal2Review soal2Review = new Soal2Review();
                //    kembali();
                //    break;
                case 5:
                    Soal3 soal3 = new Soal3();
                    kembali();
                    break;
                case 6:
                    Soal3Review soal3Review = new Soal3Review();
                    kembali();
                    break;
                default:
                    break;
            }
            Console.Write("\nPress any key...");
            Console.ReadKey();
        }

        static void kembali()
        {
            Console.WriteLine("\nApakah ingin kembali ke menu ? (y/n) ");
            string menuInput = Console.ReadLine().ToLower();
            if (menuInput == "y")
            {
                Console.Clear();
                menu();
            }
            else
            {
                Console.WriteLine("Terima kasih!");
                Console.WriteLine("Silahkan tekan Enter kembali...");
            }
        }

    }

}

