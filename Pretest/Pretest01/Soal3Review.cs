﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest01
{
    internal class Soal3Review
    {
        public Soal3Review()
        {
            Console.WriteLine("--Review Soal 3--");
            Console.Write("Input : ");
            char[] masukkan = Console.ReadLine().ToCharArray();

            string pustaka = "abcdefghijklmnopqrstuvwxyz0123456789";
            for (int i = 1; i < masukkan.Length; i++)
            {
                for (int j = i; j > 0; j--)
                {
                    //untuk sorting tanpa menggunakan sort
                    if (pustaka.IndexOf(masukkan[j]) < pustaka.IndexOf(masukkan[j - 1]))
                    {
                        var temp = masukkan[j];
                        masukkan[j] = masukkan[j - 1];
                        masukkan[j-1] = temp;
                    }
                }
            }
            foreach (var item in masukkan)
            {
                Console.Write(item);
            }

        }
    }
}
