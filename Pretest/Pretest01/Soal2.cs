﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest01
{
    internal class Soal2
    {
        public Soal2()
        {
            Console.WriteLine("--Soal 2--");
            Console.Write("Input kata: ");
            string kata = Console.ReadLine().ToLower();
            Console.Write("Input kalimat : ");
            string kalimat = Console.ReadLine().ToLower();

            int index = 0;

            for (int i = 0; i < kalimat.Length; i++)
            {
                if (index < kata.Length && kalimat[i] == kata[index])
                {
                    index++;
                }
            }
            if (index == kata.Length)
            {
                Console.WriteLine("YES");
            }
            else
            {
                Console.WriteLine("NO");
            }
        }
    }
}
