﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest04
{
    internal class Soal2
    {
        public Soal2()
        {
            Console.Write("Inputan (pakai spasi): "); 
            int[] arrInp = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

            int angka = 0;
            int angkaGan = 0;
            for (int i = 0; i < arrInp.Length; i++)
            {
                if (arrInp[i] % 2 == 0)
                {
                    angka += arrInp[i];
                }
                else if ( arrInp[i] % 2 == 1)
                {
                    angkaGan += arrInp[i];
                }
            }
            int hasil = angka - angkaGan;
            Console.WriteLine($"Hasil : Hasil Genap {angka} - Hasil Ganjil {angkaGan} = {hasil}");
        }
    }
}
