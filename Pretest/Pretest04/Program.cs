﻿namespace Pretest04
{
    public class Program
    {
        public Program()
        {
            menu();
        }
        static void Main(string[] args)
        {
            menu();
        }
        static void menu()
        {
            Console.WriteLine("==== Welcome to Pretest 04 ====");
            Console.WriteLine("|   1. Soal 1                  |");
            Console.WriteLine("|   2. Soal 2                  |");
            Console.WriteLine("|   3. Soal 3                  |");
            Console.WriteLine("|   4. Soal 4                  |");
            Console.WriteLine("|   6. Soal 6                  |");
            Console.WriteLine("|   0. Exit		       |");
            Console.Write("Masukkan no soal: ");
            int soal = int.Parse(Console.ReadLine());

            switch (soal)
            {
                case 2:
                    Soal2 soal2 = new Soal2();
                    kembali();
                    break;
                case 3:
                    Soal3 soal3 = new Soal3();
                    kembali();
                    break;
                case 4:
                    Soal4 soal4 = new Soal4();
                    kembali();
                    break;
                case 5:
                    Soal5 soal5 = new Soal5();
                    kembali();
                    break;
                case 6:
                    Soal6 soal6 = new Soal6();
                    kembali();
                    break;
                default:
                    break;
            }
            Console.Write("\nPress any key...");
            Console.ReadKey();
        }

        static void kembali()
        {
            Console.WriteLine("\nApakah ingin kembali ke menu ? (y/n) ");
            string menuInput = Console.ReadLine().ToLower();
            if (menuInput == "y")
            {
                Console.Clear();
                menu();
            }
            else
            {
                Console.WriteLine("Terima kasih!");
                Console.WriteLine("Silahkan tekan Enter kembali...");
            }
        }

    }

}

