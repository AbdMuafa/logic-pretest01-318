﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest04
{
    internal class Soal3
    {
        public Soal3()
        {
            Console.Write("Inputan (pakai spasi): ");
            int[] arrInp = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

            for (int i = 0; i < arrInp.Length; i++)
            {
                for (int j = 0; j < i; j++)
                {
                    arrInp[j]++;
                }
            }
        }
    }
}
