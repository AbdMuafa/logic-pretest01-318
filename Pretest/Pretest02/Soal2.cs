﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest02
{
    internal class Soal2
    {
        public Soal2()
        {
            int input = 0;
            Console.Write("input : ");
            input = int.Parse(Console.ReadLine());

            // * * * * *
            // * *   * *
            // *   5   *
            // * *   * *
            // * * * * *

            for (int i = 1; i <= input; i++)
            {
                for (int j = 1; j <= input; j++)
                {
                    if (i == 1)
                    {
                        Console.Write("*\t");
                    }
                    else if (i == input)
                    {
                        Console.Write("*\t");
                    }
                    else if (j == 1 || j == input)
                    {
                        Console.Write("*\t");
                    }
                    else if (i == j)
                    {
                        if (i == 3 || j == 3)
                        {
                            Console.Write(input+"\t");
                        }
                        else
                        {
                            Console.Write("*\t");
                        }
                    }
                    else if (i == (j / 2) || j == i / 2)
                    {
                        Console.Write("*\t");
                    }
                    else
                    {
                        Console.Write("\t");
                    }
                }
                Console.WriteLine("");
            }
        }
    }
}
