﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest02
{
    internal class Soal3
    {
        public Soal3()
        {
            Console.Write("Jam masuk : ");
            string[] dtAwal = Console.ReadLine().Split('T');
            string[] dateAwal = dtAwal[0].Split('-');
            string[] timeAwal = dtAwal[1].Split(':');
            DateTime awal = new DateTime(int.Parse(dateAwal[0]), int.Parse(dateAwal[1]), int.Parse(dateAwal[2]), int.Parse(timeAwal[0]), int.Parse(timeAwal[1]), int.Parse(timeAwal[2]));

            Console.Write("Jam keluar: ");
            string[] dtAkhir = Console.ReadLine().Split('T');
            string[] dateAkhir = dtAkhir[0].Split('-');
            string[] timeAkhir = dtAkhir[1].Split(':');
            DateTime akhir = new DateTime(int.Parse(dateAkhir[0]), int.Parse(dateAkhir[1]), int.Parse(dateAkhir[2]), int.Parse(timeAkhir[0]), int.Parse(timeAkhir[1]), int.Parse(timeAkhir[2]));

            //TimeSpan diff = DateTime.Now - dateTimeAkhir;
            TimeSpan diff = akhir - awal;

            int jam = (int)(diff.TotalHours);
            double tarif = 0;

            Console.WriteLine($"Selisih jam : {jam}");

            if (jam % 24 == 0)
            {
                tarif = 20000;
            }
            else if(jam > 0)
            {
                tarif = 5000;
            }
            else if (jam > 1 && jam <= 7 )
            {
                tarif = 5000 + (3000 * jam);
            }
            else if(jam > 7 && jam <= 12)
            {
                tarif = 5000 + (3000 * jam) + (2000 * jam % 5);
            }
                Console.Write($"total jam {jam}, tarif Rp.{tarif}");
        }
    }
}
