﻿namespace Pretest03
{
    public class Program
    {
        public Program()
        {
            menu();
        }
        static void Main(string[] args)
        {
            menu();
        }
        static void menu()
        {
            Console.WriteLine("==== Welcome to Pretest 03 ====");
            Console.WriteLine("|   1. Soal 7                  |");
            Console.WriteLine("|   2. Soal 8                  |");
            Console.WriteLine("|   3. Soal 9                  |");
            Console.WriteLine("|   0. Exit		       |");
            Console.Write("Masukkan no soal: ");
            int soal = int.Parse(Console.ReadLine());

            switch (soal)
            {
                case 1:
                    Soal7 soal7 = new Soal7();
                    kembali();
                    break;
                case 2:
                    Soal8 soal8 = new Soal8();
                    kembali();
                    break;
                default:
                    break;
            }
            Console.Write("\nPress any key...");
            Console.ReadKey();
        }

        static void kembali()
        {
            Console.WriteLine("\nApakah ingin kembali ke menu ? (y/n) ");
            string menuInput = Console.ReadLine().ToLower();
            if (menuInput == "y")
            {
                Console.Clear();
                menu();
            }
            else
            {
                Console.WriteLine("Terima kasih!");
                Console.WriteLine("Silahkan tekan Enter kembali...");
            }
        }

    }

}

